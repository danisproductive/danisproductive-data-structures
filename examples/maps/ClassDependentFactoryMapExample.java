package maps;

import com.danisproductive.datastructures.classes.ClassCreator;
import com.danisproductive.datastructures.maps.ClassDependentFactoryMap;

public class ClassDependentFactoryMapExample {
    public static void main(String[] args) {
        /*
         * ================================
         *      Initialize factory
         * ================================
         */
        ClassDependentFactoryMap<Shape, Point> shapeFactory = new ClassDependentFactoryMap<>();

        shapeFactory.put(Circle.class, (position) -> new Circle(position, 0));

        ClassCreator<Rectangle, Point> rectangleCreator = (position) -> new Rectangle(position, 0, 0);
        shapeFactory.put(Rectangle.class, rectangleCreator);

        /*
         * ================================
         *         Utilize factory
         * ================================
         */
        Point initialPosition = new Point(0, 0);
        shapeFactory.create(Circle.class, initialPosition);
    }
}

class Point {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}

class Shape {
    Point position;

    public Shape(Point position) {
        this.position = position;
    }

    public Point getPosition() {
        return position;
    }
}

class Circle extends Shape {
    int radius;

    public Circle(Point position, int radius) {
        super(position);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }
}

class Rectangle extends Shape {
    int height;
    int width;

    public Rectangle(Point position, int height, int width) {
        super(position);
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}