package com.danisproductive.datastructures.maps;

import com.danisproductive.datastructures.classes.ClassCreator;

import java.util.HashMap;
import java.util.Map;


/**
 * A map of factories with keys as {@link Class<? extends K>} and values of
 * {@link ClassCreator <? extends K,? extends V>}.
 * Meaning the map holds a creator for some type inheriting from {@link V} which is created from some class of type
 * {@link K}.
 * @param <K> Key for factories map and base type for input for the {@link ClassCreator}.
 * @param <V> Base type of the classes the factory will be creating.
 * @see <a href="https://gitlab.com/danisproductive/danisproductive-data-structures/-/blob/main/examples/maps/ClassDependentFactoryMapExample.java" >Usage example</a>
 */
public class ClassDependentFactoryMap <K, V> {
    Map<Class<? extends K>, ClassCreator<? extends K,? extends V>> creators;

    /**
     * Construct an empty factory map
     */
    public ClassDependentFactoryMap() {
        creators = new HashMap<>();
    }

    /**
     * Associate the given class ({@code key}) with {@link ClassCreator} {@code value}.
     * If the map previously contained the given key, the old value will be replaced.
     * @param key Key with which the specified value will be associated.
     * @param value Value to be associated with the specified key.
     * @param <T> Some type inheriting from {@link K}.
     * @param <Y> Some type inheriting from {@link V}.
     */
    public <T extends K, Y extends V> void put(Class<T> key, ClassCreator<T, Y> value) {
        creators.put(key, value);
    }

    /**
     * Get the {@link ClassCreator} to which the specified key is mapped to,
     * or {@code null} if this map contains no mapping for the key.
     * @param key Key for which to get the mapped value.
     * @param <T> Some type inheriting from {@link K}.
     * @return {@link ClassCreator} to which the specified key is mapped to, or null if there isn`t one.
     */
    public <T extends K> ClassCreator<? extends K, ? extends V> get(Class<T> key) {
        return creators.get(key);
    }

    /**
     * Invoke the create function of the {@link ClassCreator} associated with the specified key and return the created
     * instance, or null if there is no associated {@link ClassCreator} with the key.
     * @param key Key for which to invoke the create method of.
     * @return The created instance for specified key, or null if there is no associated {@link ClassCreator} with the
     * key.
     */
    @SuppressWarnings("unchecked")
    public K create(Class<? extends K> key, V value) {
        if (key != null && containsKey(key)) {
            // This cast is valid because the put function ensures the types match */
            ClassCreator<K, V> classCreator = (ClassCreator<K, V>) creators.get(key);

            return classCreator.create(value);
        }

        return null;
    }

    /**
     * Returns true if this map contains a mapping for the specified key.
     * @param key Key whose presence in this map is to be tested
     * @param <T> Some type inheriting from {@link K}.
     * @return True if this map contains a mapping for the specified key
     */
    public <T extends K> boolean containsKey(Class<T> key) {
        return creators.containsKey(key);
    }
}
