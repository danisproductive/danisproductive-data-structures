package com.danisproductive.datastructures.classes;

/**
 * A functional interface, with function {@link #create(I)} accepting data of type {@link I} and creating instance of
 * {@link O} from it.
 * @param <I> Input Type needed to create class {@link O}.
 * @param <O> Type of class to be created.
 */
@FunctionalInterface
public interface ClassCreator<O, I> {
    /**
     * Creates an instance of {@link O} from {@code input}
     * @param input - Data needed to create {@link O}
     * @return Instance of {@link O} created from given input`s values.
     */
    O create(I input);
}
